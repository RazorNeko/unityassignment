using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public Game game;
    public GameObject[] monsterType;
    public float spawnSpeed;

    private float timeSinceSpawn = 0;

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        GameObject monster = monsterType[Random.Range(0, 3)];

        if (IsItTimeToSpawn(timeSinceSpawn))
        {
            Vector3 position = new Vector3(Random.Range(-10.0f, 10.0f), 
                0.5f, Random.Range(-10.0f, 10.0f));

            Instantiate(monster, position, Quaternion.identity);
        }

        timeSinceSpawn += Time.deltaTime;
    }

    private bool IsItTimeToSpawn(float time)
    {
        float randomPick = Random.Range(0.0f, 1.0f);
        float scaledTime = timeSinceSpawn / spawnSpeed;

        if (randomPick < scaledTime)
        {
            
            game.AddMonster();
            timeSinceSpawn = 0;

            return true;
        }

        else
            return false;
    }
}
