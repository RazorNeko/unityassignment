using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    private float startTime;
    public float passedTime = 0;

    public bool isGameOver;

    private int numOfMonsters = 0;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        passedTime = Time.time - startTime;

        if (numOfMonsters >= 10)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1); ;
        }
    }

    public void AddMonster()
    {
        numOfMonsters++;
    }

    public void KillMonster()
    {
        numOfMonsters--;
    }
}
